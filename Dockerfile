# =============================================================================
# jdeathe/centos-ssh-squid
#
# Set up your network connection to use 192.168.127.1:3128 for 
# both HTTP_PROXY and HTTPS_PROXY.
# =============================================================================
FROM jdeathe/centos-ssh:2.3.0

# -----------------------------------------------------------------------------
# Install Squid
# -----------------------------------------------------------------------------
RUN rpm --rebuilddb \
	&& yum -y install \
			--setopt=tsflags=nodocs \
			--disableplugin=fastestmirror \
		ca-certificates \
		openssl \
		squid \
	&& yum clean all \
	&& sed -i \
		-e 's~^\(http_port 3128\)$~#\1~' \
		-e 's~^\(http_access deny CONNECT !SSL_ports\)$~#\1~' \
		-e 's~^\(refresh_pattern .*\)$~#\1~g' \
		/etc/squid/squid.conf \
	&& mkdir -p \
		/etc/squid/conf.d \
	&& echo 'include /etc/squid/conf.d/*.conf' \
		>> /etc/squid/squid.conf \
	&& /usr/lib64/squid/ssl_crtd \
		-c \
		-s \
		/var/lib/ssl_db \
	&& chown \
		-R \
		squid:squid \
		/var/lib/ssl_db \
	&& touch \
		/var/log/squid/cache.log \
	&& chown \
		squid:squid \
		/var/log/squid/cache.log \
	&& openssl req \
		-x509 \
		-extensions v3_ca \
		-sha256 \
		-nodes \
		-new \
		-newkey rsa:2048 \
		-days 365 \
		-subj "/C=--/ST=STATE/L=LOCALITY/O=ORGANIZATION/CN=proxy.localdomain" \
		-keyout /etc/pki/tls/certs/proxy.localdomain.pem \
		-out /etc/pki/tls/certs/proxy.localdomain.pem \
	&& openssl dhparam \
		-outform PEM \
		-out /etc/pki/tls/certs/dhparam.pem 2048

# -----------------------------------------------------------------------------
# Copy files into place
# -----------------------------------------------------------------------------
ADD src/usr/sbin \
	/usr/sbin/
ADD src/etc/services-config/squid/conf.d \
	/etc/services-config/squid/conf.d/
ADD src/etc/services-config/supervisor/supervisord.d \
	/etc/services-config/supervisor/supervisord.d/

RUN ln -sf \
		/etc/services-config/squid/conf.d/10-large-file-cache.conf \
		/etc/squid/conf.d/10-large-file-cache.conf \
	&& ln -sf \
		/etc/services-config/supervisor/supervisord.d/squid-wrapper.conf \
		/etc/supervisord.d/squid-wrapper.conf \
	&& chmod 600 \
		/etc/services-config/squid/conf.d/10-large-file-cache.conf \
	&& chmod 600 \
		/etc/services-config/supervisor/supervisord.d/squid-wrapper.conf \
	&& chmod 700 \
		/usr/sbin/squid-wrapper

EXPOSE 3126-3128

# -----------------------------------------------------------------------------
# Set default environment variables
# -----------------------------------------------------------------------------
ENV SSH_AUTOSTART_SSHD=false \
	SSH_AUTOSTART_SSHD_BOOTSTRAP=false

# -----------------------------------------------------------------------------
# Set image metadata
# -----------------------------------------------------------------------------
LABEL \
	maintainer="James Deathe <james.deathe@gmail.com>"

CMD ["/usr/bin/supervisord", "--configuration=/etc/supervisord.conf"]