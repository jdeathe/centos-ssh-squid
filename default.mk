
# Common parameters of create and run targets
define DOCKER_CONTAINER_PARAMETERS
--name $(DOCKER_NAME) \
--restart $(DOCKER_RESTART_POLICY) \
--env "SSH_AUTOSTART_SSHD=$(SSH_AUTOSTART_SSHD)" \
--env "SSH_AUTOSTART_SSHD_BOOTSTRAP=$(SSH_AUTOSTART_SSHD_BOOTSTRAP)"
endef

DOCKER_PUBLISH := $(shell \
	if [[ $(DOCKER_PORT_MAP_TCP_3126) != NULL ]]; then printf -- '--publish %s:3126\n' $(DOCKER_PORT_MAP_TCP_3126); fi; \
	if [[ $(DOCKER_PORT_MAP_TCP_3127) != NULL ]]; then printf -- '--publish %s:3127\n' $(DOCKER_PORT_MAP_TCP_3127); fi; \
	if [[ $(DOCKER_PORT_MAP_TCP_3128) != NULL ]]; then printf -- '--publish %s:3128\n' $(DOCKER_PORT_MAP_TCP_3128); fi; \
)
